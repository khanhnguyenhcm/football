<?php

Route::get('/admin/create-player',['as'=>'admin.player.create','uses' => 'PlayerController@create']);
Route::post('/admin/create-player',['as'=>'admin.player.create','uses' => 'PlayerController@store']);

Route::get('/admin/index','PlayerController@index')->name('admin.player.index');
Route::get('/delete/{playerID}', ['as' => 'admin.player.destroy', 'uses' => 'PlayerController@destroy']);

Route::get('/edit/{playerID}', ['as' => 'admin.player.edit', 'uses' => 'PlayerController@edit']);