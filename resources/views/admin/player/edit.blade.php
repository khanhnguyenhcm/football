@extends('layouts.master')
@section('content')
<div class="container">
  <h2>Edit</h2>
  
   <form action="{{route('admin.player.edit')}}" method="post">
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group row">
      <div class="col-xs-2">
        <label for="ex1">First name<span style="color: red;">*</span></label>
        <input name="firstname" class="form-control" id="ex1" type="text" required>
      </div>
      <div class="col-xs-2">
        <label for="ex2">Last name<span style="color: red;">*</span></label>
        <input  name="lastname" class="form-control" id="ex2" type="text" required>
      </div>  
      <div class="col-xs-1">     
         <p> <label for="ex3">Gender<span style="color: red;">*</span></label></p>
          <select name="gender" id="ex3">
            <option value="other" selected>Select</option>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
       </div>  
       <div class="col-xs-1">   
          <p><label for="ex4">Level<span style="color: red;">*</span></label></p>
            <select name="level" id="ex4">
              <option value="other" selected>Select</option>
              <option value="A">A</option>
              <option value="B">B</option>
              <option value="C">C</option>
            </select> 
        </div>    
      <div class="col-xs-2">
        <label for="ex5">Phone</label>
        <input name="phone" class="form-control" id="ex5" type="text">
      </div>
      <div class="col-xs-2">
        <label for="ex6">Birth</label>
        <input class="form-control" id="ex6" name="birth" type="date">
      </div>

    </div>
    <button type="submit" class="btn btn-default">Update</button>
    <button class="btn btn-default" onclick="goBack()">Back</button>
    <script>
      function goBack(){
        window.history.back();
      }
    </script>
  </form>

</div>

@endsection