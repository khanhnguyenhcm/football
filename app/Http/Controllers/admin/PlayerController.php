<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Player;

class PlayerController extends Controller
{
	public function index(){
    	$listplayers = Player::all();
    	return view('admin.player.index',['listplayers' => $listplayers]);
    }
   public function create(){
    	
    	return view('admin.player.create');
    }

    public function store(Request $req){
    	
    	$player = new Player;
    	$player->firstname = $req->get('firstname');
    	$player->lastname = $req->get('lastname');
    	$player->gender = $req->get('gender');
    	$player->phone = $req->get('phone');
    	$player->birthday = $req->get('birth');

    	$player->level = $req->get('level');
    	$player->save();
    	return redirect()->route('admin.player.index');
    }
    public function destroy($id){

        Player::destroy($id);
        return redirect()->route('admin.player.index');
    }

    public function edit($id){
        
        return view('admin.player.edit');
     }   
}
