<?php

namespace App\Http\Controllers\Client;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Player;

class ListPlayerController extends Controller
{
    public function index(){
    	$listplayers = Player::all();
    	return view('client.listplayer.index',['listplayers' => $listplayers]);
    }


}

