@extends('layouts.master')
@section('content')

<div class="container">

<h2>Admin List Players </h2>
  <p>List Players of NeoLab</p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th class="col-xs-1">ID</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Birthday</th>
        <th>Phone</th>
        <th class="col-xs-2">Operator</th>
      </tr>
    </thead>
    @foreach ( $listplayers as $player )
    <tbody>
      <tr>
        <td class="col-xs-1">{{$player->id}}</td>
        <td>{{$player->firstname}} {{$player->lastname}}</td>
        <td>{{$player->gender}}</td>
        <td>{{$player->birthday}}</td>
        <td>{{$player->phone}}</td>  
        <td class="col-xs-2"><a class="btn btn-danger" href="{{ route('admin.player.destroy',$player->id) }}" title="delete" onclick="return confirm('Are you sure?')">Delete</a>
        <a class="btn btn-info" href="{{ route('admin.player.edit',$player->id) }}" title="edit" data-admin-id="">Edit</a></td>      
      </tr>
    </tbody>
    @endforeach
  </table>
</div>
<p style="text-align: center;:;"><a class="btn btn-primary" title="addnew" href="/admin/create-player">Add new</a></p>

</body>
</html>



@endsection