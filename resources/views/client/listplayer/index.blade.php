@extends('layouts.master')
@section('content')

<div class="container">

<h2>List Players </h2>
  <p>List Players of NeoLab</p>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th class="col-xs-1">STT</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Birthday</th>
        <th>Phone</th>
      </tr>
    </thead>
    @foreach ( $listplayers as $player )
    <tbody>
      <tr>
        <td class="col-xs-1">{{$player->id}}</td>
        <td>{{$player->firstname}} {{$player->lastname}}</td>
        <td>{{$player->gender}}</td>
        <td>{{$player->birthday}}</td>
        <td>{{$player->phone}}</td>        
      </tr>
    </tbody>
    @endforeach
  </table>
</div>

</body>
</html>



@endsection